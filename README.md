## Computational Geometry : Line Arrangement 
Problem: Given a set L of n lines in the plane, compute their arrangement which is a planar subdivision.

Author: [Abu Awal Md Shoeb](http://www.shoeb.info), Rutgers University

**Algorithm**
- Find all intersections and store them in a list that contains both segment number and intersecting points (s1,s2, intX,intY)
- Starting point is the initial point of the line segment of the given line arrangement (if LA=2 then starting point is the begining point of the SECOND line segment)
- Continue for all intersections
    - if the cuurent segment is in INTERSECTION-LIST, add the point to the LA-Points. Chnage the value of the current LA to the intersecting LA.
- Ending point of the given LA will be determined by latest LA value.
- Finally draw pairwise lines for all LA-points that were calculated and added to the La-Points list
